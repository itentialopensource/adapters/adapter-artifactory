# Overview 

This adapter is used to integrate the Itential Automation Platform (IAP) with the Artifactory System. The API that was used to build the adapter for Artifactory is usually available in the report directory of this adapter. The adapter utilizes the Artifactory API to provide the integrations that are deemed pertinent to IAP. The ReadMe file is intended to provide information on this adapter it is generated from various other Markdown files.

## Details 
The Jfrog Artifactory adapter from Itential is used to integrate the Itential Automation Platform (IAP) with Jfrog’s Artifactory to securely store and organize all your build artifacts, ensuring they are easily accessible and traceable throughout the software development lifecycle.

With this adapter you have the ability to perform operations with Jfrog Artifactory such as:

- Retrieve Artifact
- Get Builds
- Get Build
- Promote Build


For further technical details on how to install and use this adapter, please click the Technical Documentation tab. 
