# JFrog Artifactory

Vendor: JFrog Artifactory
Homepage: [https://JFrog Artifactory.com/](https://jfrog.com/)

Product: JFrog Artifactory
Product Page: https://jfrog.com/artifactory/

## Introduction
We classify Jfrog Artifactory into the CI/CD domain as Jfrog Artifactory  is a universal artifact repository manager that provides a central hub for storing, managing, and distributing software artifacts. Artifactory streamlines the release process, accelerates software delivery, and promotes collaboration across development teams. 

## Why Integrate
The Jfrog Artifactory adapter from Itential is used to integrate the Itential Automation Platform (IAP) with Jfrog’s Artifactory to securely store and organize all your build artifacts, ensuring they are easily accessible and traceable throughout the software development lifecycle.

With this adapter you have the ability to perform operations with Jfrog Artifactory such as:

- Retrieve Artifact
- Get Builds
- Get Build
- Promote Build


## Additional Product Documentation
The [API documents for Jfrog Artifactory](https://jfrog.com/help/r/jfrog-rest-apis/artifactory-rest-apis)