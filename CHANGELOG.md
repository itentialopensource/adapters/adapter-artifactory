
## 0.4.5 [10-15-2024]

* Changes made at 2024.10.14_20:38PM

See merge request itentialopensource/adapters/adapter-artifactory!14

---

## 0.4.4 [08-22-2024]

* update dependencies and metadata

See merge request itentialopensource/adapters/adapter-artifactory!12

---

## 0.4.3 [08-14-2024]

* Changes made at 2024.08.14_18:54PM

See merge request itentialopensource/adapters/adapter-artifactory!11

---

## 0.4.2 [08-07-2024]

* Changes made at 2024.08.06_20:06PM

See merge request itentialopensource/adapters/adapter-artifactory!10

---

## 0.4.1 [08-06-2024]

* Changes made at 2024.08.06_09:59AM

See merge request itentialopensource/adapters/adapter-artifactory!9

---

## 0.4.0 [05-08-2024]

* 2024 Adapter Migration

See merge request itentialopensource/adapters/devops-netops/adapter-artifactory!8

---

## 0.3.3 [03-26-2024]

* Changes made at 2024.03.26_14:23PM

See merge request itentialopensource/adapters/devops-netops/adapter-artifactory!7

---

## 0.3.2 [03-11-2024]

* Changes made at 2024.03.11_13:22PM

See merge request itentialopensource/adapters/devops-netops/adapter-artifactory!6

---

## 0.3.1 [02-26-2024]

* Changes made at 2024.02.26_13:46PM

See merge request itentialopensource/adapters/devops-netops/adapter-artifactory!5

---

## 0.3.0 [12-29-2023]

* Adapter Engine has been updated and the changes are being migrated to the adapter

See merge request itentialopensource/adapters/devops-netops/adapter-artifactory!4

---

## 0.2.0 [05-22-2022]

* Migration to the latest Adapter Foundation

See merge request itentialopensource/adapters/devops-netops/adapter-artifactory!3

---

## 0.1.2 [06-28-2021]

- Changes to sampleProperties and healthcheck

See merge request itentialopensource/adapters/devops-netops/adapter-artifactory!2

---

## 0.1.1 [03-01-2021]

- Migration to bring up to the latest foundation
  - Change to .eslintignore (adapter_modification directory)
  - Change to README.md (new properties, new scripts, new processes)
  - Changes to adapterBase.js (new methods)
  - Changes to package.json (new scripts, dependencies)
  - Changes to propertiesSchema.json (new properties and changes to existing)
  - Changes to the Unit test
  - Adding several test files, utils files and .generic entity
  - Fix order of scripts and dependencies in package.json
  - Fix order of properties in propertiesSchema.json
  - Update sampleProperties, unit and integration tests to have all new properties.
  - Add all new calls to adapter.js and pronghorn.json
  - Add suspend piece to older methods

See merge request itentialopensource/adapters/devops-netops/adapter-artifactory!1

---
